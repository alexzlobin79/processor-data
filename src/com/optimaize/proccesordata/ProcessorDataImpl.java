package com.optimaize.proccesordata;

import java.util.Objects;

public class ProcessorDataImpl implements IProcessorData {

    private Number min;

    private Number max;

    private Number average;

    private int count = 0;

    public Number getSmallestValue() {
        return min;
    }

    public Number getLargestValue() {
        return max;
    }

    public Number getAverageValue() {
        return average;
    }

    public void perform(Number value) {

        Objects.requireNonNull(value);

        if (compareNumbers(max, value) < 0) {

            this.max = value;
        }

        if (compareNumbers(value, min) < 0) {

            this.min = value;
        }

        average = (value.doubleValue() + (average == null ? 0 : average.doubleValue() * count)) / (++count);

    }

    private int compareNumbers(Number value1, Number value2) {

        if (value2 == null || value1 == null) {

            return -1;
        }

        Double num1 = value1.doubleValue();
        Double num2 = value2.doubleValue();
        return num1.compareTo(num2);
    }
}
