package com.optimaize.proccesordata;

public interface IProcessorData {

    Number getSmallestValue();

    Number getLargestValue();

    Number getAverageValue();

     void perform(Number value);
}

