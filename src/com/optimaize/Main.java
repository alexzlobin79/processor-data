package com.optimaize;

import com.optimaize.proccesordata.ProcessorDataImpl;

import java.math.BigDecimal;

public class Main {
    private static ProcessorDataImpl processorData = new ProcessorDataImpl();

    public static void main(String[] args) {

        setData();
        outputStatistic();

    }

    private static void outputStatistic() {

        outputData(processorData.getSmallestValue(), "Smallest:");

        outputData(processorData.getLargestValue(), "Largest:");

        outputData(processorData.getAverageValue(), "Average:");

    }

    private static void setData() {

        processorData.perform(new BigDecimal(0));
        processorData.perform(new Integer(25));
        processorData.perform(25F);
        processorData.perform(20);
        processorData.perform(30L);
        processorData.perform(10F);
        processorData.perform(-10);
        processorData.perform(-20.0);

    }

    private static void outputData(Number value, String text) {

        System.out.println(String.format(text + "%s", value));

    }
}
