package com.optimaize;

import com.optimaize.proccesordata.IProcessorData;
import com.optimaize.proccesordata.ProcessorDataImpl;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ProcessorDataTest {


    private static final List<IProcessorData> processorsData = new ArrayList<>();


    private static Number[][] sets = {{20.0, 10F, 10L, new Double(10), -20}, {10.0}, {}};

    @BeforeClass
    public static void init() {

        for (Number[] set : sets) {
            IProcessorData processorData = new ProcessorDataImpl();

            processorsData.add(processorData);

            for (Number value : set) {

                processorData.perform(value);

            }

        }

    }

    @Test
    public void getSmallestValue() {

        Assert.assertEquals(-20.0, processorsData.get(0).getSmallestValue().doubleValue(), 0);
        Assert.assertEquals(10.0, processorsData.get(1).getSmallestValue().doubleValue(), 0);
        Assert.assertNull( processorsData.get(2).getSmallestValue());

    }

    @Test
    public void getLargestValue() {

        Assert.assertEquals(20.0, processorsData.get(0).getLargestValue().doubleValue(), 0);
        Assert.assertEquals(10.0, processorsData.get(1).getLargestValue().doubleValue(), 0);
        Assert.assertNull(processorsData.get(2).getLargestValue());

    }

    @Test
    public void getAverageValue() {

        Assert.assertEquals(6.0, processorsData.get(0).getAverageValue().doubleValue(), 0);
        Assert.assertEquals(10.0, processorsData.get(1).getAverageValue().doubleValue(), 0);
        Assert.assertNull(processorsData.get(2).getAverageValue());

    }
}